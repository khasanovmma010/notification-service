from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet

router = DefaultRouter()
router.register("clients", ClientViewSet)


urlpatterns = []
urlpatterns += router.urls
