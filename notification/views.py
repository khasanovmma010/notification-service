from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from .models import Client
from .serializers import ClientSerializer



class ClientViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated,]
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    http_method_names = ["get", "post", "put", "delete"]
