from rest_framework import serializers
from django.core.validators import RegexValidator

from .models import Client


class ClientSerializer(serializers.ModelSerializer):
    phoen_number_validate = RegexValidator(
        regex="^7[0-9]{10}", message="phone number must be 7XXXXXXXXXX"
    )
    utc_validate = RegexValidator(
        regex="^UTC[+?-][0-9]{1,2}", message="UTC need to be started from UTC"
    )

    phone = serializers.CharField(max_length=11, validators=[phoen_number_validate])
    utc = serializers.CharField(max_length=6, validators=[utc_validate])

    class Meta:
        model = Client
        fields = ["id", "phone", "operator_code", "tag", "utc"]
        read_only_fields = ["id",]
