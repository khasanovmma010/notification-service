from django.contrib import admin
from django.contrib import admin
from .models import Client, Notification, Message


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id','phone','operator_code','tag','utc')


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('id','start_datetime','end_datetime','message_text',)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'sending_datetime', 'sending_status', 'notification_id', 'client')