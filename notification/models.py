from django.db import models
from django.utils import timezone


class Notification(models.Model):
    start_datetime = models.DateTimeField(default=timezone.now)
    message_text = models.TextField()
    end_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.message_text)


class Client(models.Model):
    phone = models.CharField(max_length=11)
    operator_code = models.IntegerField()
    tag = models.CharField(max_length=20)
    utc = models.CharField(max_length=6)

    def __str__(self):
        return str(self.phone)


class Message(models.Model):
    sending_datetime = models.DateTimeField(default=timezone.now)
    sending_status = models.CharField(max_length=30)
    notification_id = models.ForeignKey(
        "notification.Notification", on_delete=models.PROTECT
    )
    client = models.ForeignKey("notification.Client", on_delete=models.PROTECT)

    def __str__(self):
        return str(self.notification_id.message_text)
